#!/usr/bin/env python3
# Copyright (c) 2020 The Bitcoin Cash Node developers
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.

####################################################################
# corgi                                                            #
#                                                                  #
# A framework for testing emergent properties of a bitcoin network #
####################################################################               

# Main author: mtrycz (2020)
# This framework is inspired by and dedicated to aphyr


# PREREQUISITES

# an account at aws (don't use this same account for other work, you might loose it)
# ~/.aws/credentials file (or any other valid credentials in https://boto3.amazonaws.com/v1/documentation/api/latest/guide/configuration.html#guide-configuration)
# build/src/bitcoind prebuilt
#
# OPTIONAL if you run on mainnet:
# A recent snapshot of a pruned blockchain, zipped and downloadable at the location in blkchain_dl_link

import cleanup
import configs
import gather
import prepare_aws
import prepare_bitcoind
import process_data
import utils
import boto3
from botocore.exceptions import NoCredentialsError


# some globals
regional_clients = dict()
security_groups = dict()


def run_full_test():
    try:
        run_id = utils.create_new_run_id()

        utils.logprogress("Phase 0: Remember that running this will cost you money!")
        prepare_the_test()
        
        utils.logprogress("Phase 1: Setting up AWS shenanigans", 10)
        regional_clients = prepare_aws.create_aws_regional_clients()
        prepare_aws.create_security_groups(regional_clients, run_id, security_groups)

        utils.logprogress("Phase 2: Creating aws instances around the world", 60 + configs.num_nodes * 2)
        prepare_aws.create_aws_instances(run_id, regional_clients, security_groups, configs.num_nodes)

        utils.logprogress("Phase 3: Installing and running bitcoind and scripts", 500)
        prepare_bitcoind.prepare_nodes(run_id)
        prepare_bitcoind.connect_nodes(run_id, configs.num_nodes, configs.num_connections)

        utils.logprogress("Phase 4: Generating UTXOs for easy spending", 120 + configs.num_nodes * 5)
        prepare_bitcoind.prepare_utxos(run_id)
        prepare_bitcoind.copy_and_run_node_actions(run_id)

        utils.logprogress("Phase 5: The tests are running, also some blocks will be generated. Skip this with ctrl+C", 960)
        try:
            prepare_bitcoind.solve_some_blocks(run_id)
        except KeyboardInterrupt as e:
            pass

        utils.logprogress("Phase 6: Gathering and processing log data, run time is dependent on bandwidth")
        gather.gather()
        process_data.process_all()
        process_data.create_charts()
    
    except Exception as error:
        print(error)
        raise error
    
    finally:
        utils.logprogress("End Phase: Destroying all the instances")
        cleanup.cleanup()
        

def prepare_the_test():

    try:
        dummy_client = boto3.client("ec2", "eu-central-1")
        dummy_client.describe_regions()
    except NoCredentialsError as error:
        print("AWS credentials not found. Check https://boto3.amazonaws.com/v1/documentation/api/latest/guide/configuration.html#guide-configuration")
        raise error

    if configs.i_accept_the_responsibility_of_running_this_script != True:
        raise Exception("With great power comes great responsibility. Do you accept it?")

run_full_test()


# WARINING - END OF THE WORLD AHEAD
# DON'T UNCOMMENT THE FOLLOWING LINE
# cleanup.armageddon()
